# LENGUAJES DE  PROGRAMACIÓN Y ALGORITMOS
## 1.Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    :depth(1) {
        BackGroundColor white
    }
    :depth(3) {
        BackGroundColor white
    }
}
</style>
* Programando ordenadores \n en los 80 y ahora
** Sistemas
*** Sistemas\n Antiguos
**** Conocer a fondo\n la Arquitectura
**** Recursos limitados en\n Hardware
**** Lenguaje de Bajo Nivel
***** Eficientes al ejecutar
***** Menor grado de asbtración
***** Mejor uso de los recursos
*** Sistemas\n Actuales
**** No requiere de conocer la Arquitectura
**** Hardware mas potente
**** Lenguaje de Alto Nivel
***** Mayor nivel de abstracción
***** Codigo es portable entre maquinas
** ¿Qué ha cambiado?
*** Mejor velocidad de CPU y GPU
*** Programas mas complejos
*** Maquinas más potentes
*** Lenguajes con mayor nivel de asbtracción
@endtmindmap
```
## 2.Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    :depth(1) {
        BackGroundColor white
    }
    :depth(3) {
        BackGroundColor white
    }
}
</style>
* Hª de los algoritmos y de \n los lenguajes de programación
** Algoritmo
***_ es
**** Lista bien definida,ordenada y\n definida de operaciones para la\n resolucion de problemas
** Programa
***_ es
**** Conjunto de operaciones especificadas
** Lenguaje de programación
*** Paradigmas
**** Imperativo
*****_ consiste
****** Secuencia claramente definida\n de instrucciones para un ordenador
**** Orientados a objetos
*****_ consiste
****** Un modelo centrado en torno a\n objetos que tienen atributos y\n comportamientos unicos
**** Programación Funcional
*****_ consiste
****** Es un paradigma declarativo que\n se basa en un modelo matemático de\n composición de funciones
**** Programación Lógica
*****_ consiste
****** En la aplicación de las reglas\n de la lógica para inferir conclusiones\n a partir de datos.
*** Compilación
**** Compilados
*****_ son
****** El programa fuente se traduce una\n vez obteniendo un archivo que contiene\n el código máquina
**** Interpretados
*****_ son
****** Traduce el programa fuente cada vez\n que se ejecute, por lo que se necesita\n que el intérprete lo ejecute


@endtmindmap
```
## 3.Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    :depth(1) {
        BackGroundColor white
    }
    :depth(3) {
        BackGroundColor white
    }
}
</style>
* Tendencias actuales en los \n lenguajes de programación
** Lenguaje Natural
***_ es
**** Aquel que utilizamos cotidianamente\n (Español, Inglés)
** Paradigma
***_ es
**** Un modelo para resolver problemas computacionales.
***_ tipos
**** Programación Estructurada
*****_ consiste
****** En mejorar la claridad, calidad y el\n tiempo de desarrollo de un programa de\n computadora recurriendo únicamente a\n subrutinas y tres estructuras básicas:\n secuencia, selección e iteración.
*****_ ejemplos de\n lenguajes
****** ALGOL
****** Pascal
**** Programación Orienta a Objetos
*****_ consiste
****** Un modelo centrado en torno a\n objetos que tienen atributos y\n comportamientos unicos
*****_ ejemplos de\n lenguajes
****** C++
****** Java
****** C#
**** Programación Funcional
*****_ consiste
****** Es un paradigma declarativo que\n se basa en un modelo matemático de\n composición de funciones
*****_ ejemplos de\n lenguajes
****** LISP
****** ML
****** Haskell
****** Erlang
**** Programación Lógica
*****_ consiste
****** En la aplicación de las reglas\n de la lógica para inferir conclusiones\n a partir de datos.
*****_ ejemplos de\n lenguajes
****** Prolog
****** Lisp
****** Erlang
**** Programación Distribuida
*****_ consiste
****** Enfocado en desarrollar sistemas distribuidos,\n abiertos, escalables, transparentes y\n tolerantes de fallos
*****_ ejemplos de\n lenguajes
****** Ada
****** Alef
****** Erlang
****** Limbo
**** Programación Orientado a Agentes
*****_ consiste
****** En la construcción del software centrada en el concepto de agentes de software
@endtmindmap
```